from django.db import models
from job.models import Job
from job.models.applicant import Application


class Question(models.Model):
    RADIO = 'radio'
    CHECK_BOX = 'check box'
    SHORT_FORM = 'short form'
    LONG_FORM = 'long form'
    SCALE = 'scale'
    question_types = [
        (RADIO, RADIO.title()), (CHECK_BOX, CHECK_BOX.title()), (SCALE, SCALE.title()),
        (SHORT_FORM, SHORT_FORM.title()), (LONG_FORM, LONG_FORM.title())
    ]
    question_title = models.CharField(max_length=255, null=True, blank=True)
    question_text = models.TextField(null=True, blank=True)
    question_type = models.CharField(max_length=255, choices=question_types, default=SHORT_FORM)

    def __str__(self):
        return f'{self.question_title}'


class QuestionChoice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='question_choices')
    value = models.CharField(max_length=255, null=True, blank=True)
    choice_text = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.value} for {self.question.question_title}'


class JobQuestion(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='job_questions')
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name='questions')
    required = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.question.question_title} for {self.job.job_title}'


class ApplicantAnswer(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name='application_answers')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='question_answers')
    answer = models.CharField(max_length=255, null=True, blank=True)
    extended_answer = models.TextField(null=True, blank=True)

    def __str__(self):
        return f'{self.application.applicant.first_name}\'s answer ' \
               f'"{self.answer}" to {self.question.question_title}'
