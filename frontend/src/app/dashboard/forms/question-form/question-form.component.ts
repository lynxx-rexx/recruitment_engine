import { Component, OnInit } from '@angular/core';
import {questionTypes} from "../../../job/models/question.model";
import {NbDialogRef, NbDialogService} from "@nebular/theme";
import {ChoiceFormComponent} from "../choice-form/choice-form.component";
import {MatTableDataSource, MatTableModule} from "@angular/material/table";

export interface choice {
  choice: string;
}

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})
export class QuestionFormComponent implements OnInit {
  displayedColumns: string[] = ['choice', 'action'];
  dataSource = new MatTableDataSource<choice>();
  choices: choice[] = [];

  edit: boolean = false;
  questionTitle: string;
  questionText: string;
  questionType: string;
  questionTypes: any[] = [];

  constructor(private dialogService: NbDialogService,
              private dialogRef: NbDialogRef<any>) {
    const questions = Object.values(questionTypes);
    const questionsKeys = Object.keys(questionTypes);
    questions.map((value, index) => {
      this.questionTypes.push({key: questionsKeys[index], display: value});
    })
  }

  crudChoice(option: string, element: any = null) {
    if (option === 'add') {
      this.dialogService.open(ChoiceFormComponent).onClose.subscribe(newChoice => {
        if (newChoice) {
          this.choices.push({'choice': newChoice});
          this.dataSource.data = this.choices;
        }
      });
    } else if (option === 'update') {
      this.dialogService.open(ChoiceFormComponent, {
        context: {existingChoice: element.choice},
      }).onClose.subscribe(newChoice => {
        if (newChoice) {
          const index = this.choices.findIndex(choice => choice === element);
          this.choices[index].choice = newChoice;
          this.dataSource.data = this.choices;
        }
      });
    } else if (option === 'delete') {
      this.choices = this.choices.filter(choice => choice.choice !== element.choice);
      this.dataSource.data = this.choices;
    }
  }

  close() {
    const localChoices: string[] = [];
    this.choices.forEach(choice => {
      localChoices.push(choice.choice);
    })
    this.dialogRef.close({
        choices: localChoices,
        questionTitle: this.questionTitle,
        questionText: this.questionText,
        questionType: this.questionType,
    })
  }

  ngOnInit(): void {
    if (this.edit) {
      this.dataSource.data = this.choices;
    }
  }

}
