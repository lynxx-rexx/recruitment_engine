import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './components/profile/profile.component';
import { SavedJobsComponent } from './components/saved-jobs/saved-jobs.component';



@NgModule({
  declarations: [
    ProfileComponent,
    SavedJobsComponent
  ],
  imports: [
    CommonModule
  ]
})
export class AccountModule { }
