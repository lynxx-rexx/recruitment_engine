from .views import *
from rest_framework import routers
from django.urls import path, include

router = routers.DefaultRouter(trailing_slash=True)
router.register(r'job', JobViewSet, basename='job')
router.register(r'paginated-job', PaginatedJobViewSet, basename='paginated-job')
router.register(r'organisation', OrganisationViewSet, basename='organisation')
router.register(r'application', ApplicationViewSet, basename='application')
router.register(r'applicant-answer', ApplicantAnswerViewSet, basename='applicant-answer')
router.register(r'applicant-answer-question', ApplicantAnswerQuestionViewSet, basename='applicant-answer-question')
router.register(r'question', QuestionViewSet, basename='question')
router.register(r'question-choice', QuestionChoiceViewSet, basename='question-choice')
router.register(r'job-question', JobQuestionViewSet, basename='job-question')
router.register(r'job-and-question', JobAndQuestionViewSet, basename='job-and-question')

urlpatterns = [
    path('job/', include((router.urls, 'job'), namespace='job_api'))
]
