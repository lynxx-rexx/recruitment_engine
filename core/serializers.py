from rest_framework import serializers

from core.models import CustomUser


class CustomUserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        return CustomUser.objects.create_user(
            email=validated_data['email'],
            phone=validated_data.get('phone', None),
            first_name=validated_data['first_name'],
            last_name=validated_data.get('last_name', None),
            password=validated_data['password']
        )

    class Meta:
        model = CustomUser
        fields = ('email', 'first_name', 'last_name', 'phone', 'password')
