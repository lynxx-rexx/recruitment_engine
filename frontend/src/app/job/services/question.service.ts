import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ApplicantAnswer, QuestionAnswer} from "../models/question.model";
import {Observable} from "rxjs";
import {Application} from "../models/applicant.model";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  postApplicantAnswer(applicationAnswers:ApplicantAnswer[]): Observable<ApplicantAnswer[]>{
    return this.http.post<ApplicantAnswer[]>(`${environment.apiUrl}api/job/applicant-answer/`, applicationAnswers)
  }
}
