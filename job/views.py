from rest_framework import permissions

from recruitment_engine.pagination import AngularPaginator
from .serializers import *
from rest_framework.viewsets import ModelViewSet

import logging

logger = logging.getLogger(__name__)


class IsLoggedInOrReadOnly(permissions.BasePermission):
    def has_permission(self, request, view):
        if view.action == 'list' or view.action == 'retrieve':
            return True
        else:
            return bool(request.user)


class JobViewSet(ModelViewSet):
    queryset = Job.objects.all()
    permission_classes = [IsLoggedInOrReadOnly]
    serializer_class = JobSerializer


class PaginatedJobViewSet(ModelViewSet):
    queryset = Job.objects.all().order_by('job_title')
    pagination_class = AngularPaginator
    permission_classes = [IsLoggedInOrReadOnly]
    serializer_class = JobSerializer
    filter_fields = {
            'salary_from': ['exact', 'gte'],
            'salary_to': ['exact', 'lte'],
            'position_type': ['exact', 'in'],
            'date_published': ['exact', 'date__gte'],
        }


class OrganisationViewSet(ModelViewSet):
    queryset = Organisation.objects.all()
    serializer_class = OrganisationSerializer


class ApplicationViewSet(ModelViewSet):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer
    filter_fields = ['job']

    def perform_create(self, serializer):
        serializer.save(applicant=self.request.user)


class ApplicantAnswerViewSet(ModelViewSet):
    queryset = ApplicantAnswer.objects.all()
    serializer_class = ApplicantAnswerSerializer
    filter_fields = ['application']

    def get_serializer(self, *args, **kwargs):
        try:
            kwargs['many'] = isinstance(kwargs['data'], list)
        except KeyError:
            logger.warning('Applicant Answer has no \'data\' in kwargs. (GET request)')
        return super().get_serializer(*args, **kwargs)


class ApplicantAnswerQuestionViewSet(ModelViewSet):
    queryset = ApplicantAnswer.objects.all()
    serializer_class = ApplicantAnswerQuestionSerializer
    filter_fields = ['application']


class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class QuestionChoiceViewSet(ModelViewSet):
    queryset = QuestionChoice.objects.all()
    serializer_class = QuestionChoiceSerializer


class JobQuestionViewSet(ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobWithQuestionSerializer


class JobAndQuestionViewSet(ModelViewSet):
    queryset = JobQuestion.objects.all()
    serializer_class = JobAndQuestionSerializer

