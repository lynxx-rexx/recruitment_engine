import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from "./pages.component";
import {DashboardComponent} from "../job/components/dashboard/dashboard.component";
import {AuthGuard} from "../auth/auth-guard.service";
import {OverviewComponent} from "../dashboard/components/overview/overview.component";
import {JobApplyComponent} from "../job/components/job-apply/job-apply.component";
import {QuestionComponent} from "../dashboard/components/question/question.component";
import {ProfileComponent} from "../account/components/profile/profile.component";
import {SavedJobsComponent} from "../account/components/saved-jobs/saved-jobs.component";

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: DashboardComponent,
    },
    {
      canActivate: [AuthGuard],
      path: 'job/:id/apply',
      component: JobApplyComponent,
    },
    {
      canActivate: [AuthGuard],
      path: 'profile',
      component: ProfileComponent,
    },
    {
      canActivate: [AuthGuard],
      path: 'saved',
      component: SavedJobsComponent,
    },
    {
      canActivate: [AuthGuard],
      path: 'submitted',
      component: OverviewComponent,
    },
    {
      canActivate: [AuthGuard],
      path: 'create',
      component: QuestionComponent,
    },
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: '**', redirectTo: 'dashboard'},
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
