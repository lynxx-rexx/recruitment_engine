import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PositionQuestionComponent } from './position-question.component';

describe('PositionQuestionComponent', () => {
  let component: PositionQuestionComponent;
  let fixture: ComponentFixture<PositionQuestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PositionQuestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PositionQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
