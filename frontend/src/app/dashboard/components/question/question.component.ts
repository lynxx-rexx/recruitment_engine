import { Component, OnInit } from '@angular/core';
import {Job} from "../../../job/models/job.model";
import {Options} from "@angular-slider/ngx-slider";
import {NbDialogService} from "@nebular/theme";
import {choice, QuestionFormComponent} from "../../forms/question-form/question-form.component";
import {MatTableDataSource} from "@angular/material/table";
import {JobService} from "../../../job/services/job.service";
import {chainedInstruction} from "@angular/compiler/src/render3/view/util";
import {Router} from "@angular/router";

export interface QuestionFromDialog {
  id: string;
  choices: string[];
  questionTitle: string;
  questionText: string;
  questionType: string;
}

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  jobFormData = new Job();
  questions: QuestionFromDialog[];
  displayColumns = ['questionTitle', 'questionText', 'questionType', 'choices', 'action'];
  displayColumnsSummary = ['questionText', 'questionType', 'choices'];
  dataSource = new MatTableDataSource<QuestionFromDialog>();
  options: Options = {
    floor: 0,
    ceil: 200,
  };

  constructor(private dialogService: NbDialogService,
              private jobService: JobService,
              private router: Router) { }

  ngOnInit(): void {
    this.questions = [];
    this.jobFormData.salary_from = 50;
    this.jobFormData.salary_to = 150;
  }

  crudQuestion(action: string, element: any = null) {
    if (action === 'create') {
      this.dialogService.open(QuestionFormComponent).onClose.subscribe(response => {
        if (response) {
          this.questions.push(response);
          this.dataSource.data = this.questions;
        }
      });
    } else if (action === 'update') {
      const formQuestions: choice[] = [];
      element.choices.forEach((choice: string) => {
        formQuestions.push({choice: choice});
      })
      this.dialogService.open(QuestionFormComponent, {
        context: {
          choices: formQuestions,
          questionTitle: element.questionTitle,
          questionText: element.questionText,
          questionType: element.questionType,
          edit: true,
        },
      }).onClose.subscribe(response => {
        if (response) {
          this.questions.push(response);
          this.dataSource.data = this.questions;
        }
      });
    } else if (action === 'delete') {
      this.questions = this.questions.filter(question => question.questionText !== element.questionText);
      this.dataSource.data = this.questions;
    }
  }

  createJob() {
    this.jobService.createJob(this.jobFormData).subscribe( {
      // First create question
      next: job => {
        if (this.questions.length < 1) {
          this.router.navigate(['pages/home']).catch();
        }
        this.questions.forEach(question => {
          this.jobService.createQuestion(question.questionText, question.questionType, question.questionTitle).subscribe({
            next: response => {
              // If choices, create choices too
              if (question.choices.length >= 1) {
                question.choices.forEach(choice => {
                  this.jobService.createQuestionChoice(response.id, choice, '').subscribe({
                    next: () => {
                      this.router.navigate(['pages/home']).catch()
                    },
                    error: res => console.log(res),
                  });
                })
              }
              // Then link question with application
              this.jobService.createJobQuestion(job.id, response.id).subscribe({
                next: () => {},
                error: res => console.log(res),
              }) // TODO: Add in a checkbox to indicate on the question modal whether or not a question is mandatory
            },
            error: res => console.log(res),
          })
        })
      },
      error: res => console.log(res),
    });
  }

}
