import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ApplicantAnswer, Question, questionTypes} from "../../../models/question.model";

@Component({
  selector: 'app-position-question',
  templateUrl: './position-question.component.html',
  styleUrls: ['./position-question.component.scss']
})
export class PositionQuestionComponent {

  @Input() question: Question
  @Input() required: Boolean
  @Input() applicantAnswer!: ApplicantAnswer;
  @Output() applicantAnswerChange = new EventEmitter<ApplicantAnswer>();
  questionTypes = questionTypes

  constructor() {
  }

  onCheckedChange(value: string) {
    if (this.applicantAnswer.answer === undefined)
      this.applicantAnswer.answer = [value]
    else {
      const index = this.applicantAnswer.answer.findIndex((answer: string) => answer === value)
      index === -1 ? this.applicantAnswer.answer.push(value) : this.applicantAnswer.answer.splice(index, 1)
    }
  }
}

