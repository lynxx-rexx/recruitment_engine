from core.models import CustomUser
from job.models import Job
from django.db import models


class Application(models.Model):
    applicant = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='applications')
    job = models.ForeignKey(Job, on_delete=models.CASCADE, related_name='applications')
    application_date = models.DateField(auto_now_add=True)
    resume = models.FileField(null=True, blank=True, upload_to='resumes')
    cover_letter = models.FileField(null=True, blank=True, upload_to='cover_letters')

    def __str__(self):
        return f'{self.applicant.first_name} {self.applicant.last_name} application for {self.job.job_title}'
