import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Job, JobWithApplicant, JobWithQuestion} from "../models/job.model";
import {ApplicationApplicant} from "../models/applicant.model";
import {QuestionFromDialog} from "../../dashboard/components/question/question.component";
import {stringify} from "@angular/compiler/src/util";

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  getJobList(){
    return this.http.get<JobWithApplicant[]>(`${environment.apiUrl}api/job/job/`)
  }
  getPaginatedJobListEndpoint(){
    return `${environment.apiUrl}api/job/paginated-job/`
  }
  getJob(id: string){
    return this.http.get<JobWithApplicant>(`${environment.apiUrl}api/job/job/${id}/`)
  }
  getApplications(jobId: string) {
    return this.http.get<ApplicationApplicant[]>(`${environment.apiUrl}api/job/application/?job=${jobId}`)
  }
  getJobWithQuestions(id: string){
    return this.http.get<JobWithQuestion>(`${environment.apiUrl}api/job/job-question/${id}/`)
  }
  deleteApplication(applicationId: string) {
    return this.http.delete(`${environment.apiUrl}api/job/application/${applicationId}/`);
  }
  createJob(jobData: Job) {
    // const formKeys = Object.keys(jobData) as Array<keyof typeof jobData>
    // TODO: Very temporary solution that works, but it's late and patience is wearing out, come back to this tomorrow
    return this.http.post<Job>(`${environment.apiUrl}api/job/job/`, {
      job_title: jobData.job_title,
      job_description: jobData.job_description,
      start_date: jobData.start_date ? jobData.start_date.toISOString().split('T')[0] : null,
      end_date: jobData.end_date ? jobData.end_date.toISOString().split('T')[0] : null,
      close_date: jobData.close_date ? jobData.close_date.toISOString().split('T')[0] : null,
      location: jobData.location,
    });
  }
  createQuestion(questionText: string, questionType: string, questionTitle: string) {
    questionType = questionType.toLowerCase();
    questionType = questionType.replace('_', ' ');
    return this.http.post<QuestionFromDialog>(`${environment.apiUrl}api/job/question/`, {
      question_title: questionTitle,
      question_text: questionText,
      question_type: questionType,
    })
  }
  createQuestionChoice(questionId: string, value: string, choice_text: string) {
    return this.http.post(`${environment.apiUrl}api/job/question-choice/`, {
      question: questionId,
      value: value,
      choice_text: choice_text,
    })
  }
  createJobQuestion(jobId: string, questionId: string, required: boolean = false) {
    console.log(jobId, questionId);
    return this.http.post(`${environment.apiUrl}api/job/job-and-question/`, {
      question: questionId,
      job: jobId,
      required: required,
    })
  }
}
