from core.models import CustomUser
from rest_framework import permissions
from rest_framework.viewsets import ModelViewSet
from core.serializers import CustomUserSerializer


class CustomUserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = CustomUserSerializer
