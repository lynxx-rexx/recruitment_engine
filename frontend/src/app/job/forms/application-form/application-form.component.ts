import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Application} from "../../models/applicant.model";

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {

  @Input() application!: Application
  @Output() applicationChange = new EventEmitter<Application>();

  constructor() {
  }

  ngOnInit(): void {
  }

  onCoverLetterChange(event: any) {
    this.application.cover_letter = event.target.files[0]
  }
  onResumeChange(event: any) {
    this.application.resume = event.target.files[0]
  }
}
