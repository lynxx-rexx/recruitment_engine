import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {ApplicantAnswer, QuestionAnswer} from "../../models/question.model";
import {JobQuestion} from "../../models/job.model";

@Component({
  selector: 'app-position-form',
  templateUrl: './position-form.component.html',
  styleUrls: ['./position-form.component.scss']
})
export class PositionFormComponent implements OnChanges, OnInit {
  @Input() jobQuestions: JobQuestion[]
  @Output() questionAnswersChange = new EventEmitter<QuestionAnswer[]>();

  questionAnswers: QuestionAnswer[]
  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    if (this.jobQuestions == undefined) return
    this.questionAnswers = this.jobQuestions.map(jobQuestion => {
      let questionAnswer = new QuestionAnswer()
      questionAnswer.question = jobQuestion.question
      questionAnswer.applicantAnswer = new ApplicantAnswer()
      questionAnswer.required = jobQuestion.required
      return questionAnswer
    })
  }
}
