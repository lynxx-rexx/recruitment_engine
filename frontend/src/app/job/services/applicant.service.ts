import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Applicant, Application} from "../models/applicant.model";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApplicantService {

  constructor(private http: HttpClient) {
  }

  postApplicant(applicant: Applicant): Observable<Applicant> {
    return this.http.post<Applicant>(`${environment.apiUrl}api/job/applicant/`, applicant)
  }

  postApplication(application: any ): Observable<Application> {
    // I don't like this 'any' but I can't get it to work otherwise. Will revisit
    const formData = new FormData();
    for (let applicationKey in application) formData.append(applicationKey, application[applicationKey])
    return this.http.post<Application>(`${environment.apiUrl}api/job/application/`, formData)
  }
}
