import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule,
    NbRadioModule,
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbSidebarModule, NbSelectModule
} from "@nebular/theme";
import {RouterModule} from "@angular/router";
import { PositionFormComponent } from './forms/position-form/position-form.component';
import { PositionQuestionComponent } from './forms/position-form/position-question/position-question.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ApplicationFormComponent } from './forms/application-form/application-form.component';
import { JobApplyComponent } from './components/job-apply/job-apply.component';
import { JobFilterComponent } from './components/job-filter/job-filter.component';
import { JobTileComponent } from './components/job-tile/job-tile.component';
import {PaginatorModule} from "../paginator/paginator.module";



@NgModule({
  declarations: [
    PositionFormComponent,
    DashboardComponent,
    PositionFormComponent,
    PositionQuestionComponent,
    ApplicationFormComponent,
    JobApplyComponent,
    PositionFormComponent,
    JobFilterComponent,
    JobTileComponent,
  ],
  imports: [
    CommonModule,
    NbInputModule,
    NbDatepickerModule.forRoot(),
    NbButtonModule,
    NbCardModule,
    RouterModule,
    FormsModule,
    NbRadioModule,
    NbCheckboxModule,
    ReactiveFormsModule,
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    NbSidebarModule,
    NbSelectModule,
    PaginatorModule,
  ]
})
export class JobModule { }
