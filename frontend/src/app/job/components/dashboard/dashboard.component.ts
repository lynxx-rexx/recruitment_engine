import {Component, OnInit} from '@angular/core';
import {Job} from "../../models/job.model";
import {JobService} from "../../services/job.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  jobList: Job[];
  paginatedJobList: Job[] = [];
  paginatedJobListEndpoint:string = this.jobService.getPaginatedJobListEndpoint()
  filters:string[] = []
  salaryTo:number
  salaryFrom:number
  listedTime:string
  workType:string


  constructor(private jobService: JobService) {
  }

  ngOnInit(): void {
  }

  onSalaryToChange(value:number){
    this.salaryTo = value
    this.constructFilterArray()
  }

  onSalaryFromChange(value:number){
    this.salaryFrom = value
    this.constructFilterArray()
  }

  onWorkTypeChange(value:string[]){
    this.workType = value.toString()
    this.constructFilterArray()
  }

  onListedTimeChange(value:string){
    this.listedTime = value
    this.constructFilterArray()
  }

  constructFilterArray(){
    let newFilters = []
    if (this.salaryTo && this.salaryTo != -1) newFilters.push(`salary_to__lte=${this.salaryTo}`)
    if (this.salaryFrom) newFilters.push(`salary_from__gte=${this.salaryFrom}`)
    if (this.listedTime) newFilters.push(`date_published__date__gte=${this.listedTime}`)
    if (this.workType) newFilters.push(`position_type__in=${this.workType}`)
    this.filters = newFilters
  }
}
