import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverviewComponent } from './components/overview/overview.component';
import {
  NbAccordionModule,
  NbButtonModule,
  NbCardModule, NbDatepickerModule,
  NbIconModule, NbInputModule,
  NbLayoutModule, NbRadioModule,
  NbSidebarModule, NbStepperModule
} from "@nebular/theme";
import { QuestionComponent } from './components/question/question.component';
import { QuestionFormComponent } from './forms/question-form/question-form.component';
import {NgxSliderModule} from "@angular-slider/ngx-slider";
import {FormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
import { ChoiceFormComponent } from './forms/choice-form/choice-form.component';



@NgModule({
  declarations: [
    OverviewComponent,
    QuestionComponent,
    QuestionFormComponent,
    ChoiceFormComponent,
  ],
    imports: [
        CommonModule,
        NbLayoutModule,
        NbCardModule,
        NbAccordionModule,
        NbIconModule,
        NbSidebarModule,
        NbButtonModule,
        NbInputModule,
        NbDatepickerModule,
        NbStepperModule,
        NgxSliderModule,
        FormsModule,
        NbRadioModule,
        MatTableModule,
    ]
})
export class DashboardModule { }
