import {Application} from "./applicant.model";

export class Question {
  id:string
  question_title: string;
  question_text: string;
  question_type: 'radio' | 'check_box' | 'short_form' | 'long_form' | 'scale';
  question_choices: QuestionChoice[];
}

export class QuestionChoice {
  question: string;
  value: string;
  choice_text: string;
}

export class ApplicantAnswer {
  application: string;
  question: string;
  answer: any;
  extended_answer: string;
}

export class ApplicantQnA {
  application: string;
  question: Question;
  answer: any;
  extended_answer: string;
}

export class QuestionAnswer {
  question: Question;
  applicantAnswer: ApplicantAnswer;
  required:boolean;
}



export const questionTypes = {
  RADIO: 'Radio',
  CHECK_BOX: 'Check Box',
  SHORT_FORM: 'Short form',
  LONG_FORM: 'Long form',
  SCALE: 'Scale'
}
