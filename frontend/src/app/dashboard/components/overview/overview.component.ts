import { Component, OnInit } from '@angular/core';
import {Job, JobWithApplicant} from "../../../job/models/job.model";
import {JobService} from "../../../job/services/job.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {

  jobList: JobWithApplicant[];

  constructor(private jobService: JobService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.jobService.getJobList().subscribe({
      next: res => {
        this.jobList = res;
        this.jobList.forEach(job => {
          this.fetchApplications(job);
        })
      },
      error: res => console.log(res),
    })
  }

  fetchApplications(job: JobWithApplicant) {
    this.jobService.getApplications(job.id).subscribe({
      next: res => job.applications = res,
      error: res => console.log(res),
    })
  }

  convertDatetimeString(input: any) {
    return new Date(input).toLocaleDateString();
  }

  capitalise(name: string) {
    return name[0].toUpperCase() + name.substring(1, name.length)
  }

  crudApplicant(applicantId: string, job: JobWithApplicant, operation: string) {
    if (operation === 'delete') {
      this.jobService.deleteApplication(applicantId).subscribe({
        next: () => this.fetchApplications(job),
        error: res => console.log(res),
      })
    }
  }

}
