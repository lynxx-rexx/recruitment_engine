import { Component, OnInit } from '@angular/core';
import {NbDialogRef} from "@nebular/theme";

@Component({
  selector: 'app-choice-form',
  templateUrl: './choice-form.component.html',
  styleUrls: ['./choice-form.component.scss']
})
export class ChoiceFormComponent implements OnInit {

  existingChoice: string;

  choice: string;

  constructor(protected dialogRef: NbDialogRef<any>) { }

  ngOnInit(): void {
    if (this.existingChoice) {
      this.choice = this.existingChoice;
    }
  }

  close() {
    this.dialogRef.close(this.choice);
  }

}
