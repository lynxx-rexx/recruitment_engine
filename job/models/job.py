from django.db import models


class Organisation(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    description = models.CharField(max_length=255, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    contact = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.name


class Job(models.Model):
    CASUAL = 'casual'
    CONTRACT = 'contract'
    PART_TIME = 'part time'
    FULL_TIME = 'full time'
    position_types = [
        (CASUAL, CASUAL.title()), (CONTRACT, CONTRACT.title()),
        (PART_TIME, PART_TIME.title()), (FULL_TIME, FULL_TIME.title())
    ]
    job_title = models.CharField(max_length=255)
    job_description = models.TextField()
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE, null=True, blank=True)
    date_published = models.DateTimeField(auto_now_add=True)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    close_date = models.DateField(null=True, blank=True)
    location = models.CharField(max_length=255, null=True, blank=True)
    position_type = models.CharField(max_length=255, choices=position_types, default=FULL_TIME)
    salary_from = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    salary_to = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    # closed = models.BooleanField(default=False) TODO: Could be a good field in case some jobs are closed prematurely

    def __str__(self):
        return self.job_title
