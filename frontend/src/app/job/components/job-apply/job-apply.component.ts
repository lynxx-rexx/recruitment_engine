import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {JobService} from "../../services/job.service";
import {QuestionService} from "../../services/question.service";
import {NbDialogService} from "@nebular/theme";
import {JobWithQuestion} from "../../models/job.model";
import {ApplicantAnswer, QuestionAnswer} from "../../models/question.model";
import {Application} from "../../models/applicant.model";
import {ApplicantService} from "../../services/applicant.service";

@Component({
  selector: 'app-job-apply',
  templateUrl: './job-apply.component.html',
  styleUrls: ['./job-apply.component.scss']
})
export class JobApplyComponent implements OnInit {

  jobQuestion: JobWithQuestion;
  questionAnswers: QuestionAnswer[] = []
  application: Application = new Application()

  constructor(private route: ActivatedRoute,
              private jobService: JobService,
              private questionService: QuestionService,
              private dialogService: NbDialogService,
              private applicantService: ApplicantService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.jobService.getJobWithQuestions(params['id']).subscribe({
        next: res => this.jobQuestion = res,
        error: res => console.log(res)
      })
    })
  }

  onSubmit(dialog: TemplateRef<any>) {
    this.application.job = this.jobQuestion.id
    this.applicantService.postApplication(this.application).subscribe({
      next: res => {
        const applicantAnswers: ApplicantAnswer[] = this.questionAnswers.map(questionAnswer => {
          if (questionAnswer.applicantAnswer.answer instanceof Array)
            questionAnswer.applicantAnswer.answer = questionAnswer.applicantAnswer.answer.toString()
          let applicantAnswer = questionAnswer.applicantAnswer
          applicantAnswer.question = questionAnswer.question.id;
          applicantAnswer.application = res.id;
          return applicantAnswer
        })
        this.questionService.postApplicantAnswer(applicantAnswers).subscribe({
          next: res => {
            this.dialogService.open(dialog, {context: `Application Submitted!`}).onClose.subscribe({
              next: () => this.router.navigate(['page/dashboard']).catch()
            })
          },
          error: res => console.log(res),
        })
      },
      error: res => console.log(res),
    })
  }
}
