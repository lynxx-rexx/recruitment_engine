from core.serializers import CustomUserSerializer
from .models import *
from rest_framework import serializers


class OrganisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organisation
        fields = '__all__'


class JobSerializer(serializers.ModelSerializer):
    organisation = OrganisationSerializer(read_only=True)

    class Meta:
        model = Job
        fields = '__all__'


class QuestionChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionChoice
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    question_choices = QuestionChoiceSerializer(read_only=True, many=True)

    class Meta:
        model = Question
        fields = '__all__'


class ApplicantAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicantAnswer
        fields = '__all__'


class ApplicantAnswerQuestionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True)

    class Meta:
        model = ApplicantAnswer
        fields = '__all__'


class JobQuestionSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True)

    class Meta:
        model = JobQuestion
        fields = '__all__'


class JobWithQuestionSerializer(serializers.ModelSerializer):
    questions = JobQuestionSerializer(read_only=True, many=True)

    class Meta:
        model = Job
        fields = '__all__'


class ApplicationSerializer(serializers.ModelSerializer):
    applicant = CustomUserSerializer(read_only=True)
    application_answers = ApplicantAnswerSerializer(read_only=True, many=True)

    class Meta:
        model = Application
        fields = '__all__'


class JobAndQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobQuestion
        fields = '__all__'
