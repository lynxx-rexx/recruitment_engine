import {Question} from "./question.model";
import {Application, ApplicationApplicant} from "./applicant.model";
import {Paginator} from "../../paginator/models/paginator.model";

export class Organisation {
  id: string;
  name: string;
  description: string;
  address: string;
  contact: string;
}

class JobBase {
  id: string;
  job_title: string;
  job_description: string;
  date_published: Date;
  start_date: Date;
  end_date: Date;
  close_date: Date;
  location: string;
  position_type: 'casual' | 'contract' | 'part_time' | 'full time';
  salary_from: number;
  salary_to: number;
}

export class Job extends JobBase {
  organisation: string;
}

export class JobPaginated extends Paginator{
  results:Job[];
}

export class JobWithApplicant extends JobBase {
  applications: ApplicationApplicant[];
  organisation: string;
}

export class JobQuestion {
  id: string;
  question: Question;
  required: boolean;
  job: string;
}

export class JobWithQuestion extends JobBase {
  questions: JobQuestion[];
  organisation: Organisation;
}


/* TODO: Delete position related models */
export class Position {
  id: string
  job: string;
  position_title: string;
  position_description: string;
  position_type: 'casual' | 'contract' | 'part_time' | 'full time';
  salary: number;
}

export class PositionWithQuestions {
  id: string
  job: Job;
  position_title: string;
  position_description: string;
  position_type: 'casual' | 'contract' | 'part_time' | 'full time';
  salary: number;
  questions: Question[]
}

export const position_types = {
  CASUAL: 'casual',
  CONTRACT: 'contract',
  PART_TIME: 'part time',
  FULL_TIME: 'full time'
}



