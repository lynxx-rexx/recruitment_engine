import {ApplicantQnA, Question} from "./question.model";

export class Applicant {
  id:string;
  first_name: string;
  last_name: string;
  email: string;
  phone: string;
}

abstract class ApplicationBase{
  id:string;
  application_date: Date;
  resume: File;
  cover_letter: File;
}

export class Application extends ApplicationBase{
  applicant: string;
  job: string;
}

export class ApplicationApplicant extends ApplicationBase{
  applicant: Applicant;
  job: string;
  application_answers: ApplicantQnA[];
}
