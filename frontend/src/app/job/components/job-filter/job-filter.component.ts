import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {position_types} from "../../models/job.model";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-job-filter',
  templateUrl: './job-filter.component.html',
  styleUrls: ['./job-filter.component.scss']
})
export class JobFilterComponent implements OnInit {

  @Output() workTypeChange = new EventEmitter<string[]>();
  @Output() salaryFromChange = new EventEmitter<number>();
  @Output() salaryToChange = new EventEmitter<number>();
  @Output() listedTimeChange = new EventEmitter<string>();

  salaryFromOptions = ['$0', ...Array.from({length: 18}, (_, i) => `$${(i + 3) * 10}k`)]
  salaryToOptions = [...Array.from({length: 18}, (_, i) => `$${(i + 3) * 10}k`), '$200k+']
  position_types = position_types
  listedTimes = [
    {value: -1, label: 'Any time'},
    {value: new Date(), label: 'Today'},
    {value: new Date().setDate(new Date().getDate() - 3), label: 'Last 3 days'},
    {value: new Date().setDate(new Date().getDate() - 7), label: 'Last 7 days'},
    {value: new Date().setDate(new Date().getDate() - 14), label: 'Last 14 days'},
    {value: new Date().setDate(new Date().getDate() - 30), label: 'Last 30 days'},
  ]

  listingDateFrom:string

  constructor(private datePipe: DatePipe) {
  }

  ngOnInit(): void {
  }

  onDateChange(date: string | null) {
    this.listingDateFrom = <string>this.datePipe.transform(date, 'yyyy-MM-dd')
    this.listedTimeChange.emit(this.listingDateFrom)
  }

  onSalaryStringToNum(event: string) {
   return event.includes('+') ? -1 : +event.replace( /\D/g, '')*1000
  }
}
