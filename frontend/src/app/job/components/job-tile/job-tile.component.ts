import {Component, Input, OnInit} from '@angular/core';
import {Job, JobWithApplicant} from "../../models/job.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-job-tile',
  templateUrl: './job-tile.component.html',
  styleUrls: ['./job-tile.component.scss']
})
export class JobTileComponent implements OnInit {
  @Input() job:Job
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  convertDatetimeString(input: any) {
    return new Date(input).toLocaleDateString();
  }

  applyJob(jobId:string) {
    this.router.navigate([`pages/job/${jobId}/apply`]).catch();
  }
}
