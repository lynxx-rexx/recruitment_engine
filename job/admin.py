from django.contrib import admin
from core.models import CustomUser
from job.models import *

admin.site.register(Job)
admin.site.register(Organisation)
admin.site.register(CustomUser)
admin.site.register(Application)
admin.site.register(ApplicantAnswer)
admin.site.register(Question)
admin.site.register(QuestionChoice)
admin.site.register(JobQuestion)
