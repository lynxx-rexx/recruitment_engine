from rest_framework import routers
from .views import CustomUserViewSet
from django.urls import path, include
from rest_framework.authtoken import views


router = routers.DefaultRouter()
router.register(r'user-task', CustomUserViewSet, basename='user-task')

urlpatterns = [
    path('core/', include((router.urls, 'core'), namespace='api')),
    path('core/login/', views.obtain_auth_token),
]
